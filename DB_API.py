# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 16:45:12 2021

@author: David Nguyen and Sirine Aoudj

This class is used to simplify the use of queries and connecting to the database
"""
import mysql.connector

class DBAPI:
    def __init__(self,db_name):
        self.__db_name = db_name
    
    #method to connect to the database
    def connect_db(self):
        try:
            self.__conn = mysql.connector.connect(
                host = 'localhost',
                user = 'root',
                password = 'Kingdragon2',
                database = self.__db_name)
        except mysql.connector.Error as err:
            print("connect_db: Failed to connect to database.")
            print("Something went wrong: {}".format(err))
            self.__conn.rollback()

    #This takes as input the columns, table name and condition to creates a query string that can be used to get data from the database        
    def executeStmt(self, columns, tb_name, condition):
        try:
            self.connect_db()
            cursor = self.__conn.cursor()
            cursor.execute("SELECT " + columns + " FROM " + tb_name + " WHERE " + condition)
            all_data = cursor.fetchall()
            
            return all_data
                
            
        except mysql.connector.Error as err:
            print("execute: Execution failed.")
            print("Something went wrong: {}".format(err))
            self.__conn.rollback()
            
#This is for testing purposes
#test = DBAPI("covid_corona_db_DAVI_SIRI")
#test.executeStmt("*", "corona_table","Country_Other = 'USA'")