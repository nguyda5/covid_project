# -*- coding: utf-8 -*-
"""
Created on Sun Mar 21 18:19:05 2021

@author: David Nguyen and Sirine Aoudj
"""
import mysql.connector
import json
import utility
import ParseData as pd

class Database:
    
    #db_connection is a method that creates a connection to mysql.
    def db_connection(self):
        try:
            self.__conn = mysql.connector.connect(
                host = 'localhost',
                user = 'root',
                password = 'Kingdragon2')
            print("Connection success!")
        except:
            print("db_connection: Connection to database has failed.")
    
    #create_database is a method that creates a database with the given datbase name.
    def create_database(self, db_name):
        try:
            cursor = self.__conn.cursor()
            cmd = "CREATE DATABASE IF NOT EXISTS "+db_name
            cursor.execute(cmd)
            print("Database "+db_name+" created successfully!")
        except:
            print("create_database: Failed to create database.")
    
    #select_db is a method that creates a connection to the selected database.
    def select_db(self, db_name):
        try:
            self.__conn = mysql.connector.connect(
                host = 'localhost',
                user = 'root',
                password = 'Kingdragon2',
                database = db_name)
        except:
            print("select_db: Failed to connect to database.")  
    
    #create_tb is a method that creates a table into the selected database.
    def create_tb(self,db_name ,table_name, table_schema):
        try:
            self.select_db(db_name)
            cursor = self.__conn.cursor()
            cursor.execute("DROP TABLE IF EXISTS " + table_name)
            cursor.execute("CREATE TABLE " + table_name + table_schema)
            print("Table " + table_name + " has been created in " + db_name)
            self.__conn.commit()
            self.__conn.close()
        except mysql.connector.Error as err:
            print("create_tb: Failed to create table.")
            print("Something went wrong: {}".format(err))
            self.__conn.rollback()
    
    #create_all_tb is a method that creates the country border table and the corona table.
    def create_all_tb(self, heading_schema):
        self.create_tb("covid_corona_db_DAVI_SIRI","country_borders_table","(id int(5), country varchar(80), neighbor varchar(80), distance int(100), PRIMARY KEY(id))")
        self.create_tb("covid_corona_db_DAVI_SIRI","corona_table",heading_schema)
    
    #insertDataFromList is a method that inserts data into the database from a list given.
    def insertDataFromList(self,tb_list , db_name, table_name, table_schema):
        try:
            
            #creates a cursor for the database
            self.select_db(db_name)
            cursor = self.__conn.cursor()
            
            #verifies if the list given is a list for the country borders
            if len(tb_list) == 198:
                
                current_id = 0
                
                for i in range(0,len(tb_list)):
                    for key in tb_list[i][next(iter(tb_list[i]))]:
                        
                        current_id += 1
                        
                        info = [] 
                        info.append(current_id)
                        
                        #renamed the countries to match the country data for corona table
                        if(next(iter(tb_list[i]))) == "People's Republic of China":
                            info.append('China')
                        elif (next(iter(tb_list[i]))) == "Democratic Republic of the Congo":
                            info.append('Congo')
                        elif (next(iter(tb_list[i]))) == "Federated States of Micronesia":
                            info.append('Micronesia')
                        elif (next(iter(tb_list[i]))) == "Eswatini (Swaziland)":
                            info.append('Eswatini')
                        elif (next(iter(tb_list[i]))) == "United States":
                            info.append("USA")
                        else:
                            info.append(next(iter(tb_list[i])))
                        
                        #appends data to a list
                        info.append(key)
                        info.append(tb_list[i][next(iter(tb_list[i]))].get(key))
                        
                        #executes statement to insert data into table
                        stmt = "INSERT INTO " + table_name + table_schema + "VALUES (%s, %s, %s, %s)"
                        cursor.execute(stmt, (info[0],info[1],info[2],info[3]))
             
            #verifies if the list given is a list for the corona table
            elif len(tb_list) == 221:
                stmt = "INSERT INTO " + table_name + table_schema + "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                cursor.executemany(stmt, tb_list)
                        
            self.__conn.commit()
            print("Succesfully inserted values")
            
        except mysql.connector.Error as err:
            print("insertDataFromList: Failed to insert into table.")
            print("Something went wrong: {}".format(err))
            self.__conn.rollback()
    #def createHeadingSchema(self, heading_list):
        
    #execute method is a method that runs the create database method and the insert data method. It basically takes care of the
    #database data for the corona table.
    def execute(self, filename1, filename2, f1_exists, f2_exists):
        util = utility.Utility()
        
        try:
            
            #variable that verifies if the tables have been created or not
            tables_created = False
            
            #verifies if file 1 exist
            if(f1_exists):
                
                #gets the data from the filename given and retrieves all data and heading
                file_data = pd.ParseData(filename1)
                file_data.getAllData()
                file_data.createHeadings()
                
                #gets the heading schema for table creation and data insertion
                heading_schema = file_data.getHeadingSchema(1)
                heading_only = file_data.getHeadingSchema(2)
                
                #creates all tables
                self.create_all_tb(heading_schema)    
                tables_created = True
                
                #insert data into database from a given list and heading
                #inserts data for all 3 days, today, yesterday and 2 days ago
                self.insertDataFromList(file_data.today_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
                self.insertDataFromList(file_data.yesterday_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
                self.insertDataFromList(file_data.yesterday2_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
            
            #verifies if file 2 exists
            if(f2_exists):
                
                #gets the data from the filename given and retrieves all data and heading
                file_data = pd.ParseData(filename2)
                file_data.getAllData()
                file_data.createHeadings()
                
                #gets the heading schema for table creation and data insertion
                heading_schema = file_data.getHeadingSchema(1)
                heading_only = file_data.getHeadingSchema(2)
                
                #if table has not been created yet, table gets created
                if tables_created == False:
                    self.create_all_tb(heading_schema)    
                
                #inserts data for all 3 days, today, yesterday and 2 days ago
                self.insertDataFromList(file_data.today_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
                self.insertDataFromList(file_data.yesterday_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
                self.insertDataFromList(file_data.yesterday2_data,"covid_corona_db_DAVI_SIRI", "corona_table", heading_only)
            
            #get lines from json file and inserts from the list created 
            dataList = util.getLines("json/country_neighbour_dist_file.json")
            self.insertDataFromList(dataList, "covid_corona_db_DAVI_SIRI","country_borders_table", "(id, country, neighbor, distance)")
                
            self.__conn.commit()
            self.__conn.close()
        except mysql.connector.Error as err:
            print("execute: Execution failed.")
            print("Something went wrong: {}".format(err))
            self.__conn.rollback()
        
        

#test = Database()

#test.db_connection()
#test.create_database("covid_corona_db_DAVI_SIRI")
#test.execute(filename1="html_data/local_page2021-03-19.html",filename2="html_data/local_page2021-03-25.html")

            
                