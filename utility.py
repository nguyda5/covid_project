# -*- coding: utf-8 -*-
"""
Created on Sun Mar 21 19:27:48 2021

@author: David Nguyen and Sirine Aoudj
This class contains all the helper methods
"""
import json
import pandas as pd
from datetime import datetime
from datetime import timedelta

class Utility:
    #This method allows to read a json file using a file name
    def getLines(self, filename):
        try:
            with open(filename) as file:
                lines = json.load(file)
                return lines
        except:
            print("Cannot read json.")
    
    #This method returns a datafram using a filename for a file
    def getDF(self, filename):
        list = self.getLines(filename)
        df = pd.DataFrame(list)
        return df
    
    #This method gives the 3 days ago date of a given date
    def getPast3Days(self, str_date):
        today_date = datetime.strptime(str_date, '%Y-%m-%d')
        three_days_ago_date = today_date - timedelta(days=3)
        str_three_days_ago_date = str(three_days_ago_date)[0:10]
        return str_three_days_ago_date
    
    #This method gives the 3 days after date of a given date
    def getNext3Days(self, str_date):
        today_date = datetime.strptime(str_date, '%Y-%m-%d')
        three_days_next_date = today_date + timedelta(days=3)
        str_three_days_next_date = str(three_days_next_date)[0:10]
        return str_three_days_next_date
    
    #This method returns the month and year when the user inputs a day only in the app
    #then it makes an array that sets that date and the 3 days after date
    def getDates(self, day_str):
        today_date = datetime.today()
        today_date_str = str(today_date)[0:8]
        
        if len(day_str) == 1:
            today_date_str += "0" + day_str
        elif len(day_str) == 2:
            today_date_str += day_str
        
        next_date = datetime.strptime(today_date_str, '%Y-%m-%d')
        next_date = next_date + timedelta(days=3)
        
        next_date_str = str(next_date)[0:10]
        
        return [today_date_str,next_date_str]
        
        
        #next_date_str = str(next_date)[0:10]

#test = Utility()
#print(test.getPast3Days('2021-03-27'))
#print(test.getNext3Days('2021-03-27'))
#print(test.getDates('10'))