# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 14:40:03 2021

@author: David Nguyen and Sirine Aoudj

This class parses the data from a .html file to get the data
"""
from bs4 import BeautifulSoup as BS
from datetime import datetime
from datetime import timedelta
import pandas as pd

class ParseData:
    
    #This class takes as input a file name that contains a scrape of the website
    def __init__(self, filename):
        self.__filename = filename
    
    #This creates a soup object using the html file given
    def getSoupObj(self):
        file = open(self.__filename, 'r')
        html_data = file.read()
        bs4Obj = BS(html_data, 'html.parser')
        self.__BSObj = bs4Obj
    
    #This class gets the data and removes all the html tags of a table
    #We use this process for each days table to get the data
    def getData(self, table_body, date):
        #This array contains all the data from the table
        data = []

        #for every row in the table we remove the spaces and commas and \n to filter the content and get only the data
        for r in range(1,len(table_body)):
            row = []
            #Before filtering the data we appaend the date of the data (date of corona data)
            row.append(date)
            for tr in table_body[r].find_all("td"):
                currentrow = tr.text.replace("\\n","").strip()
                currentrow = currentrow.replace(",","")
                currentrow = currentrow.replace("+","")
                currentrow = currentrow.replace("N/A", "0")
                if not currentrow:
                    row.append('0') 
                else:   
                    row.append(currentrow)
            #Finally we append the data to the data array
            data.append(row)
        
        #This removes all the data at the index 0 to 7 because these contains the continents names and their countries
        #which is not useful data for the analysis so discard of it
        for x in range(7):
            del data[0]      
        
        #print(data[0])
        #print(data)

        #This method finally returns the array of data that is parsed and filtered
        return data
    
    #This method allows to get all of the data from all the tables, today, yesterday and 2 days ago
    def getAllData(self):
        
        #First we get the soup object
        self.getSoupObj()
        
        #Then we find the tables using the ids of today, yesterday and 2 days ago table
        self.today_table = self.__BSObj.find(id="main_table_countries_today")
        self.yesterday_table = self.__BSObj.find(id="main_table_countries_yesterday")
        self.yesterday2_table = self.__BSObj.find(id="main_table_countries_yesterday2")
        
        #Then we get the body of these tables using tbody to get all the rows of the tables
        today_body = self.today_table.tbody.find_all(("tr"))
        yesterday_body = self.yesterday_table.tbody.find_all(("tr"))
        yesterday2_body = self.yesterday2_table.tbody.find_all(("tr"))

        #This is an array that contains the dates of today,yesterday and 2 days ago to be able to have the date for each data of each country
        dates = self.getDates(self.__filename)
        
        #Finally we use the get data method to filter the data of each table and so we pass on the bodies of each table
        #So now a parseData object contains all the data for the html file giving as input
        self.today_data = self.getData(today_body, dates[0])
        self.yesterday_data = self.getData(yesterday_body, dates[1])
        self.yesterday2_data = self.getData(yesterday2_body, dates[2])
        
        #print(self.today_data)
    
    #This method gets the headings of the table to be able to use them to organize the data
    def createHeadings(self):
        #finding the headings in the thead of the table
        head = self.today_table.thead.find_all(("tr"))
        headings = []
        #filtering and storing each cell into the headings array
        for th in head[0].find_all("th"):
            heading = th.text.replace("\\n","").strip()
            heading = heading.replace("\xa0", "al")
            heading = heading.replace(",","_")
            heading = heading.replace(" ","_")
            heading = heading.replace("/","_")
            if heading == "#":
                heading = heading.replace("#","rank")
            headings.append(heading) 
        self.headings = headings
        #Adding the date heading because it was not included in the original table of the website
        #but keeping track of the dates allows use to analyse the data better later on
        self.headings.insert(0,"date")
    
    #This allows to create heading scheme to insert the data into the database
    def getHeadingSchema(self, choice):
        schema = "("
        
        #choice 1 consist of taking the headings to create the table in the database
        if choice == 1:
            for heading in self.headings:
                
                if heading == 'rank':
                    heading = 'country_rank'
                schema += heading + " "
                
                if heading == 'date': 
                    schema += 'date,'
                elif heading == 'country_rank':
                    schema += 'int(4),'
                elif heading in ['TotalCases', 'NewCases', 'TotalDeaths','NewDeaths','TotalRecovered','NewRecovered','ActiveCases','Serious_Critical','TotalCases_1M_pop','Deaths_1M_pop','TotalTests','Tests_1M_pop','Population','1_Caseevery_X_ppl','1_Deathevery_X_ppl','1_Testevery_X_ppl']:
                    schema += 'int(20),'
                elif heading in ['Country_Other','Continent']:
                    schema += 'varchar(80),'
        #choice two consist of getting the columns for inserting the data inside the database
        elif choice == 2:
            for heading in self.headings:
                if heading == 'rank':
                    heading = 'country_rank'
                schema += heading +","
        schema = schema[:-1]
        schema += ")"

        #it returns the schema for either creating a database or for inserting inside a database depending on what the choice is
        return schema

    #This gets the date from the filename and finds the dates of the 2 previous dates to keep track of the dates with each data    
    def getDates(self, fileName):
        #Extracting the date string from the file name
        date_1_string = fileName[20:30]
        #transforming it to date data type
        date_1 = datetime.strptime(date_1_string, '%Y-%m-%d')
        
        #Finding the date of one day ago for the yesterday table and passing it as a string
        date_2 = date_1 - timedelta(days=1)
        date_2_string = str(date_2)
        date_2_string = date_2_string[0:10]
        
        #Finding the date of two day ago for the yesterday table and passing it as a string
        date_3 = date_2 - timedelta(days=1)
        date_3_string = str(date_3)
        date_3_string = date_3_string[0:10]

        #returning the array of all the dates
        dates = [date_1_string, date_2_string, date_3_string]
        return dates

#this is for testing purposes of this class
#test = ParseData("html_data/local_page2021-03-19.html")
#test.getAllData()
#test.createHeadings()
#test.getDates("local_page2021-03-19.html")

