Our project contain a folder called json that contains the json file given
to insert it into the database created.

The first python file is called Scrape data which contains everything to 
scrape the data from a url input.

The second one is called ParseData which parse the data of a given file in
this project it takes the scrape files and parse them to get the data from the tables
form today, yesterday and 2 days ago and stores the data into a variable of the class

The we have the DB_Managment file that allows to connect to a database, then create 
a database then create two table. One called corona_table tha contains
all of the data for each country with their respective dates.
Then we have country_borders_table that contains the countires with their neighbours and distance.
This class also insert all of the data into the database created.

Then we have DB_API which contaisn methods that allows to simplify the use of
queries when we try to retrieve data from the database.

Then we have utilities which basicaly contains a couple methods that facilitate
certain methods we have for other classes to maek it more organized.

Then we have Data_Science which is where we analyze all the data and create the plots
for a given date and country.


How to run our program:
1.First it is necessary to changou your username and password for the database connection
  in the following classes:  db_managment.py at lines 17-18 and 36-37
			     DB_API at the lines 20-21
2. Select the mian_Application file and run it
3. There will be two choices, 1 or 2
	enter 1 to execute a scrape that file create a html file of the local webpage
	enter 2 to choose a file
4. If you choose 2 it will let you enter a day
5. it will find the file for the days and let you know if one of the file are not available if you would like 
   to proceed anyways. 
   If you enter Y: It will then insert the data into the database for the file found.
   If you enter N: then it will end the program
6. If both of the files were found it will insert the data for both files

7.Once the data has been inserted it will let you input a country name(it is case sensitive)

8. Once you entered the country name it will prompt the 3 plots for the country then the program ends.
				
You can then take a look at the data!