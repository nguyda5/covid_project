# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 13:37:05 2021

@author: David Nguyen and Sirine Aoudj

This class scrapes a webpage to an html file
"""
from urllib.request import urlopen, Request
from datetime import date

class ScrapeData:
    
    #Gets and url as input to scrape from
    def __init__(self, url):
        self.__url = url
    
    #Request the html from the url and return the html code
    def getHTML(self):
        req = Request(self.__url, headers ={'user-agent':'Mozilla/5.0'})
        html_code = urlopen(req)
        
        self.__htmlCode = html_code

    #Saving the html returned by the request into a html file
    def saveToLocal(self):
        try:
            file = open(self.__filename, 'w')
            text_data = self.__htmlCode.read()
            html = text_data.decode("utf-8")
            file.write(str(text_data))
            file.close()
        except:
            print("An error has occured! File is unable to be written.")
    
    #Getting the date of the scrape
    def getDate(self):
        today = date.today()
        today = str(today)
        return today
    
    #Creating the file name using the date of the scrape
    def getFilename(self):
        self.__filename = "html_data/local_page" + self.getDate() + ".html"
    
    #When calling execute, it gets the html then stores it into a .html file
    def executeScrape(self):
        self.getHTML()
        print("GetHTML executed")
        self.getFilename()
        print("GetFilename executed")
        self.saveToLocal()
        print("File successfully executed")
        
#test = ScrapeData("https://www.worldometers.info/coronavirus/")
#test.executeScrape()
#html_code = test.getHTML()
#print(html_code)
