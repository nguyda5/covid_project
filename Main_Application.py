# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 18:59:09 2021

@author: David Nguyen and Sirine Aoudj

This is the main application where we can run the program to get data for the a date and country input
"""
import ScrapeData as SD
import utility as utl
import DB_Management as DB
import os.path
import Data_Science as DS

def execute():
    #First we create a DataScrapper object with the worldometers corona page since our program is made for this url by default
    data_scrapper = SD.ScrapeData('https://www.worldometers.info/coronavirus/')
    #Initialize a utility object that will be used
    util = utl.Utility()
    #Create a database object
    database = DB.Database()
    
    #Allows the user to choose from these 2 choices and storing his answer into user_choice
    print("Enter your choice:")
    print("1: save to local web file")
    print("2: scrape from a saved local file")
    user_choice = input("Enter: ")

    #removing the space from the input so it does not interfer with the conditions and convert it to an int
    user_choice = user_choice.strip(" ")
    user_choice = int(user_choice)

    #If the user choose the scrape the data it executes the scrape using the DataScraper object
    #This will create and html file into a html_data directory where the program is stored
    if user_choice == 1:
        print("Data scrapping started.")
        data_scrapper.executeScrape()
        print("Data scrapping completed.")
    #If the user choose to get data, we let the user input a day for the current month
    elif user_choice == 2:
        print("Enter day only (1-31)")
        user_answer = input("Enter: " )

        #We procced to create date strings for the date given and the date of 3 days after
        date_list = util.getDates(user_answer)     
        
        #We then creates file names for those dates
        filename1 = "html_data/local_page"+date_list[0]+".html"
        filename2 = "html_data/local_page"+date_list[1]+".html"
        

        try:
            #First we try to connect to the database using the databse object
            database.db_connection()
            #Then we create a database that will contain the data
            database.create_database("covid_corona_db_DAVI_SIRI")
            
            #First if the two files for the dates exist we insert the data of those file into the database
            if(os.path.exists(filename1) == True and os.path.exists(filename2) == True):
                print("Inserting both files into the database..")
                
                try:
                    database.execute(filename1,filename2, True, True)
                except:
                    print("Inserting data failed.")

            #If only the second date file exist we ask the user if he would like to proceed and insert the data
            # for the second file only if yes we will insert this file date into the database
            # if no then the program ends        
            elif os.path.exists(filename1) == False and os.path.exists(filename2) == True:
                print("html file for "+date_list[0]+" does not exist.")
                print("html file for "+date_list[1]+" exist.\n")
                user_choice = input("Proceed to continue? (Y/N) : ")
                user_choice = user_choice.upper()
                
                if user_choice == "Y":
                    print("Inserting into database for "+date_list[1]+" data")
                    try:
                        database.execute(filename1,filename2, False, True)
                    except:
                        print("Inserting data failed.")
                else:
                    print("Program ending..")
            #If only the first date file exist we ask the user if he would like to proceed and insert the data
            # for the first file only if yes we will insert this file date into the database
            # if no then the program ends          
            elif os.path.exists(filename2) == False and os.path.exists(filename1) == True:
                print("html file for "+date_list[1]+" does not exist.")
                print("html file for "+date_list[0]+" exist.\n")
                user_choice = input("Proceed to continue? (Y/N) : ")
                user_choice = user_choice.upper()
                if user_choice == "Y":
                    print("Inserting into database for "+date_list[0]+" data")
                    try:
                        database.execute(filename1,filename2, True, False)
                    except:
                        print("Inserting data failed.")
                else:
                    print("Program ending..")
            
            #Finally we ask the user to input a country name and store that as user_country
            user_country = input("Enter a country to analyse (case sensitive): ")
            #We then create a dataScience object that will allow to analyse the data of the country input
            data_science = DS.DataScience(user_country, date_list[0])
            #First we call execute which gets all the data for the country
            data_science.execute()
            #Then we call analyse which creates dataframes and plots the country input
            data_science.analyse()
            
        except:
            print("An error has occurred.")
        
        

                
        
execute()