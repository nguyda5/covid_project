# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 16:29:05 2021

@author: David Nguyen and Sirine Aoudj

This class uses pandas to explore, analyse and plott the data for a given country and date
"""
import DB_API 
import pandas as pd
import utility

class DataScience:

    #take a country name and a date to analyse the data with
    def __init__(self, country, user_date):
        self.__util = utility.Utility()
        self.__country = country
        #getting the dates of the past 3 days of the date given
        self.__next_date = self.__util.getNext3Days(user_date)
        #getting the dates of the next 3 days of the date given
        self.__past_date = self.__util.getPast3Days(user_date)
        
    def execute(self):
        
        #This will contain the country data for the dates
        country_data = ()
        #This will contain the data for the neighbor countries for the dates
        neighbor_data = ()
        #The dates of 3 days before and 3 days after
        date_list = []
        #contains the new cases numbers
        NewCases_list = []
        #contains the new recovered numbers
        NewRecovered_list = []
        #contains the new deaths numbers
        NewDeaths_list = []
        #contains the number of deaths per 1M numbers
        Deaths1M_list = []
        #contains the neighbor countries name
        neighbors_list = []
        
        try:
            #first we connect to the database that contains all the data
            db_name = "covid_corona_db_DAVI_SIRI"
            dbapi = DB_API.DBAPI(db_name)
            
            #define the colums for the query
            columns = "date, NewCases, NewRecovered, NewDeaths, Deaths_1M_pop"
            #define the table name for the query
            table = "corona_table"
            #define the condition for the query
            condition = "Country_Other = '" +self.__country+"' AND " + "date BETWEEN '"+self.__past_date+"' AND '"+self.__next_date+"' ORDER BY date ASC"
            
            #fiding the data inside the database that staisfies the columns,table and condition set above
            data = dbapi.executeStmt(columns, table, condition) 
            
            #Assogn this data to the country_data var
            country_data = data
            
            #This gives the number of days we have data for 
            self.__days_count = len(data)
            
            #For each row of data we get the resective data to their respective list that we defined above:
            if len(country_data) != 0:
                
                for a in range(0,len(country_data)):
                   
                    #the date
                    date_list.append(str(country_data[a][0]))
                    #the new cases number
                    NewCases_list.append(country_data[a][1])
                    #the new recovered numbers
                    NewRecovered_list.append(country_data[a][2])
                    #the new deaths number
                    NewDeaths_list.append(country_data[a][3])
                    #the deaths per 1M number
                    Deaths1M_list.append(country_data[a][4])            

                #getting all the neighbors data of that country    
                neighbor_data = self.getNeighborsData()
                #getting the number of neighbours for that country
                self.__neighbor_count = len(neighbor_data)
                
                #If the country dies have neighbours then append the neighbours data in its respective list
                if len(neighbor_data) != 0:
                    for neighbor in neighbor_data:
                        neighbors_list.append(neighbor[0])
                
                #create a country dictionary for all the data for each date 
                country_dict = {'dates': date_list, 'new_cases': NewCases_list, 'new_recovered':NewRecovered_list,'new_deaths':NewDeaths_list,'deaths_1m_pop': Deaths1M_list, 'neighbors': neighbors_list}
                #assign it to the class
                self.__country_dict = country_dict
                
                #Creating a dataframe for the country data
                self.__country_df = self.createCountryDF(country_dict)
                print("CountryDF created")
                
                #Assigning the neighbours in the disctonary to the neighbour list
                self.__neighbors_list = country_dict['neighbors']
                
                #Get the dictionary of the neighbour data
                self.getNeighborsDict(country_dict)
                
                #Get the dataframe list for the neighbours data
                self.__neighbors_df_list = []
                
                #Making dataframes depending on the number of neighbours for each neighbour
                for i in range(len(self.__neighbor_list_dict)): 
                    neighbor_name = self.__neighbors_list[i]
                    self.__neighbors_df_list.append(self.createNeighborDF(self.__neighbor_list_dict[i], neighbor_name))
                    
                print("Neighbors DF created")
        except:
            print("getCountryData: An error has occurred.")
            
    #This method gets the neighbours name and distance for the country input    
    def getNeighborsData(self):
        try:
            db_name = "covid_corona_db_DAVI_SIRI"
            dbapi = DB_API.DBAPI(db_name)
            
            columns = "neighbor, distance"
            table = "country_borders_table"
            
            #order byt disctance so we know by order that in the array of neighbour it starts with the one with the longest border to the smallest
            #so at [0] we have the neighbour with the longest border
            condition = "country = '"+self.__country+"' ORDER BY distance DESC"
            
            data = dbapi.executeStmt(columns, table, condition) 
            neighbor_data = data
            
            #return all the neighbours data
            return(neighbor_data)
        except:
            print("getNeighborsData: An error has occurred.")
    
    #This gets the actual information about a neighbour input (date, new cases and deaths per 1M) 
    def getNeighborData(self, neighbor_name):
        try:
            db_name = "covid_corona_db_DAVI_SIRI"
            dbapi = DB_API.DBAPI(db_name)
            
            columns = "date, NewCases, Deaths_1M_pop"
            table = "corona_table"
            condition = "Country_Other = '"+neighbor_name+"' AND " + "date BETWEEN '"+self.__past_date+"' AND '"+self.__next_date+"' ORDER BY date ASC"
            
            data = dbapi.executeStmt(columns, table, condition) 
            neighbor_data = data
            return(neighbor_data)
            #returns the data for the neighbor input fomr the database
        
        except:
            print("getNeighborData: An error has occurred.")
    
    #This method creates a dictionary for the neighbors
    #the dictionary contains the neighbour name, its data with dates list, new cases lists and deaths per 1M list
    def getNeighborsDict(self, country_dict):
        
        self.__neighbor_list_dict = []
        
        #for each neighbor we get use the getneighborsData() adn assign each data to their respective lists
        for neighbor in country_dict['neighbors']:
            data = self.getNeighborData(neighbor)
            
            #initialize the lists that will contains the information for each country
            date_list = []
            NewCases_list = []
            Deaths_1M_pop_list = []
            
            #For each piece of data we separete it into date, new case and deaths and append them to the list
            #First we verify if the country input has neighbours to do this process
            if len(data) >= 3:
                for a in range(0,len(data)):
                    date_list.append(str(data[a][0]))
                    NewCases_list.append(data[a][1])
                    Deaths_1M_pop_list.append(data[a][2])
                
                #finally we assign those to the dictionary    
                neighbor_dict = {'dates': date_list,'new_cases-'+neighbor: NewCases_list, 'deaths_1m_pop-'+neighbor: Deaths_1M_pop_list}
            
            #If the country does not have neighbors we append empty list as if we did not
            #It interfered when we tried to make dataframes
            else:
                empty_list = []
                for a in range(0,self.__days_count):
                    empty_list.append(0)
                    
                neighbor_dict = {'dates': empty_list, 'new_cases-'+neighbor: empty_list, 'deaths_1m_pop-'+neighbor: empty_list}
            
            #Finally we return the dictionary
            self.__neighbor_list_dict.append(neighbor_dict)
        
        print("NeighborsDict created")

    #This creates a dataframe for the country and we use the dictionary of the country to make it    
    def createCountryDF(self, user_dict):
        country_df = pd.DataFrame(user_dict, columns = ['dates','new_cases','new_recovered','new_deaths','deaths_1m_pop'])
        
        return(country_df)

    #This creates a dataframe for the neighbour using the neighbour dictionary and one neighbour name
    def createNeighborDF(self, neighbor_dict, neighbor_name):
        neighbor_df = pd.DataFrame(neighbor_dict, columns = ['dates','new_cases-'+neighbor_name,'deaths_1m_pop-'+neighbor_name])
        return(neighbor_df)
    
    #This method analyses all the data we put into the dictionaries previously
    def analyse(self):
        
        #First we are able to plot the data for the first graph as we haev all the data for the days given and the country given
        #x is the dates that haev stored in the dataframe then on the y is is the new cases, new recovered and new deaths stored into the dataframe
        #We use days counts depending on the numbers of days we haev the data for to use in the title of the graph(ex: we only have teh data for 3 days then the title would be: 3 days key indicators evolution)
        self.__country_df.plot.bar(x = 'dates', y = ['new_cases','new_recovered','new_deaths'], ylabel = 'people',title = str(self.__days_count) + " days key indicators evolution " + self.__country)
        
        #For the second graph with the closest neigbhour
        #first we establish the condition that it has to have neighbours to do these plots
        if self.__neighbor_count > 0:
            
            #we take the neighbour at 0 because it is the one with the longest border as we get them in order by the longest to the shortest
            neighbor_name = self.__neighbors_list[0]
            
            #This is to create the datafraame with the country name
            new_cases_neighbor = "new_cases-"+neighbor_name

            #We then create a merged dictionary that uses the dates from the country_dictionary but the data from the neighbour dictionary
            #we use the index 0 as the neighbour at 0 has the longest border
            merged_dict = {'dates':self.__country_dict['dates'],'new_cases-'+self.__country:self.__country_dict['new_cases'],new_cases_neighbor:self.__neighbor_list_dict[0][new_cases_neighbor]}
            #This is the merged datafram that contains the input country data but also the neighnour data
            merged_df = pd.DataFrame(merged_dict, columns = ['dates','new_cases-'+self.__country,'new_cases-'+neighbor_name])
            
            #Finally we plot using the dataframe just created once againg we uses days count to display the number of days we have the data for
            merged_df.plot.bar(ylabel = 'new_cases', x = 'dates', y = ['new_cases-'+self.__country,'new_cases-'+neighbor_name], title = str(self.__days_count) + " days New Cases comparison with neighbour " + neighbor_name)
        
        #This part consist of making the third graph with the 3 longest border neighbours and deaths per 1M
        #first we verify if the country has at least one neighbour to work with
        if self.__neighbor_count > 0:
            
            #We create a list that contains the names of the neighbours that will be used in the dataframe
            #ex: deaths_1M_pop-italy
            heading_list =[]
            heading_list.append('deaths_1m_pop-'+self.__country)
            
            #create the neighbour to contain the dates and the deaths per 1m 
            neighbors_dict = {'dates':self.__country_dict['dates']}
            neighbors_dict['deaths_1m_pop-'+self.__country] = self.__country_dict['deaths_1m_pop']
            
            #If the country given has more than 3 neighbours we only take the 3 first neighbours
            if self.__neighbor_count > 3:
                for i in range(0,3):
                    #We get the neighbours data and create a heading to add in the heading list for the dataframe
                    neighbors_dict['deaths_1m_pop-'+self.__neighbors_list[i]] = self.__neighbor_list_dict[i]['deaths_1m_pop-'+self.__neighbors_list[i]]
                    heading_list.append('deaths_1m_pop-'+self.__neighbors_list[i])
            
            #If the country has less than 3 neighbours we use the lenght of neighbours to loops through how many heading we need 
            elif self.__neighbor_count < 3:
                for i in range(0,len(self.__neighbors_list)):
                    #we get the headings for the neighbours adn the data then we append to the heading list
                    neighbors_dict['deaths_1m_pop-'+self.__neighbors_list[i]] = self.__neighbor_list_dict[i]['deaths_1m_pop-'+self.__neighbors_list[i]]
                    heading_list.append('deaths_1m_pop-'+self.__neighbors_list[i])

                  
            neighbors_df = pd.DataFrame(neighbors_dict, columns = ['dates','deaths_1m_pop-'+self.__country])
            
            #In the case we have more than 3 neighbours we know we have 3 neighbours and we can create the dataframe using the list in the range of 3
            if self.__neighbor_count > 3:
                for i in range(0,3):
                    neighbors_df['deaths_1m_pop-'+self.__neighbors_list[i]] = self.__neighbor_list_dict[i]['deaths_1m_pop-'+self.__neighbors_list[i]]
            
            #We are now able to create the dataframe using the list 
            # we do this because we cannot know for sure how many headings we need since the number of neighbours depends on the country chosen
            # we use the lenght of the list to loop trhough since we do not know the amount of neighbours  
            elif self.__neighbor_count < 3:
                for i in range(0,len(self.__neighbors_list)):
                    neighbors_df['deaths_1m_pop-'+self.__neighbors_list[i]] = self.__neighbor_list_dict[i]['deaths_1m_pop-'+self.__neighbors_list[i]]
                    
            #finally we plot the data using the dataframe created in fonction of the number of neighbours
            neighbors_df.plot.bar(x = 'dates', y = heading_list, ylabel = 'Deaths/1M pop', title = str(self.__days_count)+'days Deaths/1M pop comparison '+self.__country+" with "+str(len(heading_list)-1) + " neighbours")

        #This would happend in the case of not having any neighbours for the given counrtry then non of these plots will be created    
        else:
            print("No neighbors! Cannot create plot for analysis.")
        

#this is for testing purposes
#test2 = DataScience('USA','2021-03-25')
#test2.execute()
#test2.analyse()

        
        
    